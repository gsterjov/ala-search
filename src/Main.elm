module Main exposing (..)

import Browser exposing (Document)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Url.Builder exposing (crossOrigin)
import Json.Decode exposing (Decoder, field, maybe, int, string)
import File.Download


-- Main
-- We don't need a full blown single page application for this so we
-- only hook it up to the browser document and ignore any navigation handling
main = Browser.document { init = init, update = update, subscriptions = subscriptions, view = view }


-- Model

{-| The application state. Here we track and values that are double bound
and the results of user actions. Everything comes back to this model
so any substate should be a member of this model as well
-}
type alias Model =
  { state : SearchState
  , query : String
  , lastRequest : Maybe SearchRequest
  , lastResponse : Maybe (Result Http.Error SearchResults)
  }

{-| The current state of the application. Ideally this state should
be represented with a custom type for the user action being taken but
since this isn't a complex site we opt for a simple out of band
"state monitoring"

- Unloaded: no user action has been taken yet
- Loading: waiting for a server response
- Loaded: server response received and processed (including errors)
-}
type SearchState
    = Unloaded
    | Loading
    | Loaded

{-| A search request to the ALA servers. See the ALA API docs for more
info at https://api.ala.org.au/#ws2. We only implement a subset of the
possible requests since we only really need to paginate a generic query
-}
type alias SearchRequest =
  { query : String
  , page : Int
  }

{-| The server response from the search request. This represents the
JSON document that gets returned by the search endpoint. Its provided
here for the decoder to validate the structure.
Because the body has a root key of `searchResults` you almost always
want to deref into the `SearchResults` type
-}
type alias SearchResponse =
  { searchResults : SearchResults }

{-| A subset of the search results from the ALA servers
-}
type alias SearchResults =
  { totalRecords : Int
  , results : List Record
  }

{-| A record from the search results. The search results are strongly
typed to this record and the JSON validation will ensure values are
properly decoded otherwise a `BadBody` Http error will be returned. As
such we only decode the values needed for the actual rendering of results
-}
type alias Record =
  { commonName : Maybe String
  , scientificName : Maybe String
  , thumbnailUrl : Maybe String
  }

{-| The server response from a CSV search request. Much like `SearchResponse`
this represents the JSON document from the same search endpoint. However for
CSV generation we instead hardcode parameters to start at record 0 with a
page size of 100.
Since the CSV requires a different output as well we create a new stream
to maintain strong type checking when decoding the JSON body
-}
type alias CsvResponse =
  { searchResults : CsvResults }

{-| A subset of the search results for a CSV export
-}
type alias CsvResults =
  { totalRecords : Int
  , results : List CsvRecord
  }

{-| A record from the search results for a CSV export. This has the same
functionality as `Record` for the search functionality except that it
holds different fields that the exporter needs.
Because `id` and `guid` appear to be mandatory fields we do not need to
make it a Maybe type. If that assumption fails an appropriate error message
from the decoding failure will be rendered
-}
type alias CsvRecord =
  { id : String
  , guid : String
  , kingdom : Maybe String
  , kingdomGuid : Maybe String
  , scientificName : Maybe String
  , author : Maybe String
  , imageUrl : Maybe String
  }


-- Init
init : () -> ( Model, Cmd Msg )
init _ = ((Model Unloaded "" Nothing Nothing), Cmd.none)


-- Update

{-| The main app messaging. These are the messages received from
commands or user interaction within the app and allows for the
elm architecture to maintain separation of concerns between the
model and the view.
It is responsible for updating the model when an interaction occurs
which the elm runtime will diff and re-render the appropriate view

- Query: Updates the model with the string in the text box when changed
- DoSearch: Enter or the search button was activated
- GenerateCSV: The CSV export button was pressed
- PreviousPage: Go to the previous page on the search result view
- NextPage: Go to the next page on the search result view

- GotSearchResponse: Either an HTTP/decode error or the actual search results
- GotCsvResponse: Either an HTTP/decode error or the actual CSV search results
-}
type Msg
  = Query String
  | DoSearch
  | GenerateCSV
  | PreviousPage
  | NextPage
  | GotSearchResponse (Result Http.Error SearchResponse)
  | GotCsvResponse (Result Http.Error CsvResponse)

{-| The main update method processing all app messages. It is here where the
application logic largely takes place in response to a user event or a system
event (like an XHR response).
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    Query query ->
      ({ model | query = query }, Cmd.none)

    DoSearch ->
      -- clear out the model and kick off a new search request
      searchRequest (model, { query = model.query, page = 0 })

    GenerateCSV ->
      -- because CSV generation is available until a search succeeds we
      -- leverage the last request made and use it to kick of a specialised
      -- csv search request. We also leverage the same Loading state to
      -- render a generic loading screen to guard against double clicks
      case model.lastRequest of
        Nothing -> (model, Cmd.none)
        Just lastRequest -> ({ model | state = Loading }, (getCsv lastRequest))

    PreviousPage ->
      -- Use the last search request to decrement the page count and
      -- rerun the search
      case model.lastRequest of
        Nothing -> (model, Cmd.none)
        Just lastRequest -> searchRequest (model, (previousPage lastRequest))

    NextPage ->
      -- Use the last search request to increment the page count and
      -- rerun the search
      case model.lastRequest of
        Nothing -> (model, Cmd.none)
        Just lastRequest -> searchRequest (model, (nextPage lastRequest))

    GotSearchResponse result ->
      -- Because of the root `searchResults` key we grab the actual search
      -- results before updating the model as a convenience. The error
      -- is still preserved this way causing an alternate rendering if one
      -- occurred, and subsequently no results will be accessible.
      let lastResponse = Result.map (\resp -> resp.searchResults) result
      in
      ({ model | state = Loaded, lastResponse = Just lastResponse }, Cmd.none)

    GotCsvResponse result ->
      -- Get the specialised search results and pass them to the downloadCsv
      -- function to transform and send to the browser as a text/csv file.
      -- If an error occurred we actually replace the `lastResponse` in the
      -- model so that the error message can be rendered. Because the error
      -- view replaces the search view we don't have to worry about losing
      -- the search results in this process as a new search will have to
      -- be executed anyway.
      -- This little nuance allows us to avoid managing more states that
      -- cover CSV generation and CSV failures
      let lastResponse = Result.map (\resp -> resp.searchResults) result
      in
      case lastResponse of
        Ok response -> ({ model | state = Loaded }, downloadCsv response)
        Err err -> ({ model | state = Loaded, lastResponse = Just (Err err) }, Cmd.none)

{-| Prepare the model for a new search and kick off a search. This is mostly
a convenience function as a few messages need to rerun the search with some
minor changes. Particularly pagination
-}
searchRequest : (Model, SearchRequest) -> (Model, Cmd Msg)
searchRequest (model, request) =
  ({ model | state = Loading, lastResponse = Nothing, lastRequest = Just request }, getSearchResults request)

{-| Create a SearchRequest for the previous page. If there are no previous
pages then return a request for the current one
-}
previousPage : SearchRequest -> SearchRequest
previousPage request =
  if request.page > 0 then
    { request | page = request.page - 1 }
  else
    request

{-| Create a SearchRequest for the next page. If there are no next pages we
still increment the page number for the request. Because the pagination is
rather basic and doesn't display the current page, total pages, nor allow
jumping to specific pages, we just assume that anyone who travels too far
will execute another search
-}
nextPage : SearchRequest -> SearchRequest
nextPage request =
  { request | page = request.page + 1 }


-- Subscriptions
subscriptions model = Sub.none


-- View

{-| The main view rendering the entire document
-}
view : Model -> Document Msg
view model =
  Document "ALA Search" (body model)

{-| Renders the search form and any results
-}
body : Model -> List (Html Msg)
body model =
  [ section [ class "hero is-info" ]
    [ div [ class "hero-body" ]
      [ p [ class "title" ] [ text "ALA Search" ]
      , div [ class "columns" ] [ searchForm model ]
      ]
    ]
  , section [ class "section columns" ] [ searchResults model ]
  ]

{-| A simple form that executes a search. This also responds to the changing
state in the model, specifically the loading state
-}
searchForm : Model -> Html Msg
searchForm model =
  let isLoading = if model.state == Loading then " is-loading" else ""
  in
  Html.form [ onSubmit DoSearch ]
    [ div [ class "field is-grouped" ]
      [ p [ class ("control has-icons-left" ++ isLoading) ]
        [ input [ class "input", type_ "text", value model.query, onInput Query ] []
        , span [ class "icon is-left" ] [ node "ion-icon" [ name "search-sharp" ][] ]
        ]
      , button [ class ("button is-primary" ++ isLoading) ] [ text "Search" ]
      ]
    ]

{-| Renders the search results whether successful or not. This is the main
body of the search page and handles all possible states.
-}
searchResults : Model -> Html Msg
searchResults model =
  case model.state of
    Unloaded -> searchUnloaded
    Loading -> searchLoading
    Loaded -> case model.lastResponse of
      Nothing -> searchUnloaded -- we shouldn't ever be so don't show anything
      Just result -> case result of
        Err err -> searchFailed err -- show the error view (for csv exports as well)
        Ok results -> if results.totalRecords > 0 -- show the results if any
                      then searchResultList results
                      else searchEmpty

{-| A blank view that shows on first load
-}
searchUnloaded : Html Msg
searchUnloaded =
  div [] []

{-| A generic loading view that hides the search and error views
-}
searchLoading : Html Msg
searchLoading =
  div [ class "column columns is-centered" ]
    [ div [ class "column is-half box" ]
      [ h2 [] [ text "Loading.." ]
      , progress [ class "progress is-small is-info", Html.Attributes.max "100" ] []
      ]
    ]

{-| A simple empty results page
-}
searchEmpty : Html Msg
searchEmpty =
  div [ class "content is-large" ] [ h2 [] [ text "No results found" ] ]

{-| A simple error view that can render for any Http.Error
-}
searchFailed : Http.Error -> Html Msg
searchFailed err =
  article [ class "message is-danger" ]
    [ div [ class "message-header" ] [ p [] [ text "An error occurred" ] ]
    , div [ class "message-body" ] [ text (errorMessage err) ]
    ]

{-| Converts the Http error into a detailed string. This attempts to display
as much information about the error as possible. Because elm has no runtime
exceptions these are all the errors that can occur during a request
-}
errorMessage : Http.Error -> String
errorMessage err =
  case err of
    Http.BadUrl url -> "The URL is invalid: " ++ url
    Http.Timeout -> "Request timed out"
    Http.NetworkError -> "A network failure has occurred"
    Http.BadStatus code -> "Unexpected server response: " ++ String.fromInt(code)
    Http.BadBody str -> "JSON parsing error: " ++ str

{-| A search result view rendering the pagination panel and a list of records.
-}
searchResultList : SearchResults -> Html Msg
searchResultList lastSearch =
  div [ class "column is-full" ]
    [ div [ class "columns" ] [ div [ class "column is-full block" ] [ searchPagination lastSearch ] ]
    , div [ class "columns" ] [ div [ class "column is-full" ] (List.map recordCard lastSearch.results) ]
    ]

{-| A very basic pagination view. This also includes the option to export the current
search results as a csv (limit 100 records)
-}
searchPagination : SearchResults -> Html Msg
searchPagination lastSearch =
  nav [ class "pagination is-right is-small" ]
    [ a [ class "pagination-previous", onClick PreviousPage ] [ text "Previous" ]
    , a [ class "pagination-next", onClick NextPage ] [ text "Next" ]
    , ul [ class "pagination-list" ] [ a [ class "pagination-link is-current", onClick GenerateCSV ] [ text "Download CSV" ] ]
    ]

{-| A simple record display showing the name and a thumbnail
-}
recordCard : Record -> Html Msg
recordCard record =
  div [ class "box" ]
    [ article [ class "media" ]
      [ recordCardThumbnail record
      , div [ class "media-content" ]
        [ div [ class "content" ] [ recordCardTitle record ] ]
      ]
    ]

{-| The record content rendering the species name. To address data consistency
we render the scientific name and common names as a psuedo-table to highlight
when data is not available for the search record.
-}
recordCardTitle : Record -> Html Msg
recordCardTitle record =
  p []
    [ strong [] [ text "Scientific name: " ]
    , text (Maybe.withDefault "No designation" record.scientificName)
    , br [] []
    , strong [] [ text "Common names: " ]
    , text (Maybe.withDefault "" record.commonName)
    ]

{-| The record thumbnail. Because the thumbnail sizes vary considerably we also
set the custom css class `thumbnail` on the image to ensure it never exceeds the
96x96 extents. This doesn't resolve the performance impact of loading large images
but it does prevent unsightly breaking of bounds
-}
recordCardThumbnail : Record -> Html Msg
recordCardThumbnail record =
  let thumbnailUrl = Maybe.withDefault "" record.thumbnailUrl
  in
  figure [ class "media-left" ]
    [ p [ class "image is-96x96" ] [ img [src thumbnailUrl, class "thumbnail" ] [] ]
    ]


-- HTTP

{-| Builds a URL from the search request
-}
searchURL : SearchRequest -> String
searchURL request =
  crossOrigin "https://bie.ala.org.au" [ "ws", "search.json" ]
    [ Url.Builder.string "q" request.query
    , Url.Builder.int "start" (request.page * 10)
    , Url.Builder.int "pageSize" 10
    ]

{-| A convenience function to execute a search request. This will send the
`GotSearchResponse` message when resolved
-}
getSearchResults : SearchRequest -> Cmd Msg
getSearchResults request =
  Http.get { url = searchURL request, expect = Http.expectJson GotSearchResponse searchResponseDecoder }

{-| Builds a URL from the search request.
According to https://api.ala.org.au/#ws2 there is a species download endpoint,
however it doesn't allow constraining the records based on page size so we instead
resort to hitting the search endpoint and generating the CSV client side
-}
csvURL : SearchRequest -> String
csvURL request =
  crossOrigin "https://bie.ala.org.au" [ "ws", "search.json" ]
    [ Url.Builder.string "q" request.query
    , Url.Builder.int "start" 0
    , Url.Builder.int "pageSize" 100
    ]

{-| A convenience function to execute a csv request. This will send the
`GotCsvResponse` message when resolved
-}
getCsv : SearchRequest -> Cmd Msg
getCsv request =
  Http.get { url = csvURL request, expect = Http.expectJson GotCsvResponse csvResponseDecoder }


-- Decoders
searchResponseDecoder : Decoder SearchResponse
searchResponseDecoder =
  Json.Decode.map SearchResponse
    (field "searchResults" searchResultsDecoder)

searchResultsDecoder : Decoder SearchResults
searchResultsDecoder =
  Json.Decode.map2 SearchResults
    (field "totalRecords" int)
    (field "results" recordDecoder)

{-| The JSON decoder for search results
-}
recordDecoder : Decoder (List Record)
recordDecoder =
  Json.Decode.list (Json.Decode.map3 Record
    (maybe (field "commonName" string))
    (maybe (field "scientificName" string)) -- this appears to be an optional field also
    (maybe (field "thumbnailUrl" string))
  )

csvResponseDecoder : Decoder CsvResponse
csvResponseDecoder =
  Json.Decode.map CsvResponse
    (field "searchResults" csvResultsDecoder)

csvResultsDecoder : Decoder CsvResults
csvResultsDecoder =
  Json.Decode.map2 CsvResults
    (field "totalRecords" int)
    (field "results" csvRecordDecoder)

{-| The JSON decoder for CSV export records
-}
csvRecordDecoder : Decoder (List CsvRecord)
csvRecordDecoder =
  Json.Decode.list (Json.Decode.map7 CsvRecord
    (field "id" string)
    (field "guid" string)
    (maybe (field "kingdom" string))
    (maybe (field "kingdomGuid" string))
    (maybe (field "scientificName" string))
    (maybe (field "author" string))
    (maybe (field "imageUrl" string))
  )


-- Helpers

{-| Transforms and downloads the CSV search results.
-}
downloadCsv : CsvResults -> Cmd Msg
downloadCsv results =
  results.results
    |> csvPluck
    |> toCsv
    |> String.append "id,guid,kingdom,kingdomGuid,scientificName,author,imageUrl\n"
    |> File.Download.string "species.csv" "text/csv"

{-| Transforms a list of records into a list of lists (containing just the values).
This is not unlike `pluck` functionality in ORMs
-}
csvPluck : List CsvRecord -> List (List String)
csvPluck records =
  List.map csvValues records

{-| Transforms a record into a list of values. The values are in order
of the columns in the CSV export file
-}
csvValues : CsvRecord -> List String
csvValues record =
  [ record.id
  , record.guid
  , Maybe.withDefault "" record.kingdom
  , Maybe.withDefault "" record.kingdomGuid
  , Maybe.withDefault "" record.scientificName
  , Maybe.withDefault "" record.author
  , Maybe.withDefault "" record.imageUrl
  ]

{-| Escapes the value for use in a CSV column. Specifically it escapes
a double quote with another double quote and surrounds the value with
double quotes to allow for commas
-}
escapeCsv : String -> String
escapeCsv str =
  "\"" ++ (String.replace "\"" "\"\"" str) ++ "\""

{-| Transforms a list of values into a CSV line by joining them with a
comma delimiter
-}
toCsvLine : List String -> String
toCsvLine values =
  values
    |> List.map escapeCsv
    |> String.join ","

{-| Transforms a list of value lists into a CSV document string. This is
best used with `csvPluck`
-}
toCsv : List (List String) -> String
toCsv lines =
  lines
    |> List.map toCsvLine
    |> String.join "\n"
