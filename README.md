# Introduction

A small foray into [Elm](https://elm-lang.org/) providing very basic search functionality to the living australia database. It leverages the elm architecture which might be familiar from other frameworks (Model-View-Update as can be found in yew, redux). It also benefits greatly from being strongly typed and purely functional allowing for fast iteration.

This webapp is also hosted on gitlab pages at https://gsterjov.gitlab.io/ala-search/ where it will always reflect the `main` branch. See the `.gitlab-ci.yml` file on how to automatically build and deploy an optimised version of elm apps.

# Dependencies

First you'll need the elm compiler. Detailed instructions for most platforms can be found in the official [install guide](https://guide.elm-lang.org/install/elm.html).

In linux the easiest way to get started is to use something like [asdf](https://asdf-vm.com/) and installing the elm community plugin like so:

``` sh
asdf plugin add elm
asdf install elm 0.19.1
```

This repository includes `.tool-versions` so if asdf is properly installed for your shell it should automatically switch to the right elm version. Otherwise setting the global elm version works just as well:

``` sh
asdf global elm 0.19.1
```

Alternatively elm can be installed via npm with `npm install -g elm`. This might be less convenient however due to this repository not using nodejs at all apart from the gitlab-ci integration.

# How to build and run

Once elm is installed and ready to go compiling and using this repository can be done with a single line. In the root repository directory run:

``` sh
elm make src/Main.elm --output=public/main.js
```

This will output the transpiled JS file into the public folder alongside the `index.html` bootstrap. Simply open up the `index.html` file in a browser to load the application.

For the lazy:

``` sh
xdg-open public/index.html
```

# Further explorations

Unsurprisingly the css styling gums up the works a bit in the view functions. There are a number of elm packages that encapsulates css frameworks so that a component like `media-object` are neatly encapsulated as a view function rather than being manually constructed from divs like in this repository. See [elm-css-bulma](https://package.elm-lang.org/packages/MichaelCombs28/elm-css-bulma/latest/) as an example of what could be leveraged here.

Alternatively it would be a good exercise to encapsulate bulma components in a separate elm module to more deeply understand the package system and determine the level of complexity involved in such a task.
